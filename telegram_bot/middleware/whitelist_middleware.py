import os

from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware

from telegram_bot.service.bot_members import BotMembers


class WhitelistMiddleware(BaseMiddleware):

    async def on_process_message(self, message: types.Message, data: dict):
        if message.from_user.id not in BotMembers.get():
            await message.reply('✋ You are not in the whitelist. Your user ID is: %d' % message.from_user.id)
            raise CancelHandler()
