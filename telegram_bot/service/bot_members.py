import os


class BotMembers:

    @staticmethod
    def get():
        return map(int, os.environ.get('TELEGRAM_BOT_WHITELIST').split(','))