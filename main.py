import asyncio
import os
import time
import logging
import sys

from aiogram import Bot, Dispatcher, types
from datetime import datetime, timedelta

from aiogram.types import BotCommand
from meross_iot.controller.device import BaseDevice
from meross_iot.http_api import MerossHttpClient
from meross_iot.manager import MerossManager, T
from dotenv import load_dotenv
from meross_iot.model.plugin.power import PowerInfo

from telegram_bot.middleware.whitelist_middleware import WhitelistMiddleware
from exception.plug_connection_exception import PlugConnectionException
from telegram_bot.service.bot_members import BotMembers

load_dotenv()

EMAIL = os.environ.get('MEROSS_EMAIL')
PASSWORD = os.environ.get('MEROSS_PASSWORD')
max_seconds_low_consumption = int(os.environ.get('MAX_SECONDS_LOW_CONSUMPTION'))
consumption_threshold = int(os.environ.get('CONSUMPTION_THRESHOLD'))
fetch_seconds_interval = int(os.environ.get('FETCH_SECONDS_INTERVAL'))

device: BaseDevice = None
bot: Bot = None
manager = None
http_api_client: None


watchdog_enabled = True
last_watchdog_check = None

logging.basicConfig(level=logging.INFO)


async def telegram_command_start(event: types. Message):
    await event.answer(
        f"Hello, {event.from_user.get_mention(as_html=True)} 👋! Welcome to the MiningWatchdog Bot.",
        parse_mode=types.ParseMode.HTML,
    )


async def telegram_command_consumption(event: types. Message):
    await event.answer("Current consumption is %s W" % await get_current_consumption())


async def telegram_command_status(event: types. Message):
    await event.answer("The watchdog is %s" % ('enabled!' if watchdog_enabled else 'disabled'))


async def telegram_command_enable(event: types. Message):
    await set_watchdog_status(True)


async def telegram_command_disable(event: types. Message):
    await set_watchdog_status(False)


async def telegram_command_powercycle(event: types. Message):
    await power_cycle()


async def telegram_command_debug(event: types. Message):
    await event.answer(
        'Status: %s\n'
        'Current power consumption: %s W\n'
        'Last watchdog check: %s\n'
        % (
            ('Enabled' if watchdog_enabled else 'Disabled'),
            await get_current_consumption(),
            last_watchdog_check.strftime("%H:%M:%S")
        )
    )


async def set_watchdog_status(enabled: bool):
    global watchdog_enabled
    watchdog_enabled = enabled
    await watchdog_status_changed()


async def watchdog_status_changed():
    global watchdog_enabled
    await telegram_broadcast(
        'The status has been changed to: %s' % ('Enabled' if watchdog_enabled else 'Disabled'),
        True
    )


async def telegram_broadcast(message: str, disable_notification: bool):
    global bot
    for user_id in BotMembers.get():
        try:
            await bot.send_message(user_id, message, disable_notification=disable_notification)
        except Exception as e:
            print(e)


async def main():
    bot_task = asyncio.create_task(telegram_bot_main())
    watchdog_task = asyncio.create_task(watchdog())
    watchdog_task.add_done_callback(handle_watchdog_termination)

    print(f"started at {time.strftime('%X')}")

    await bot_task
    await watchdog_task


async def telegram_bot_main():
    global bot
    bot = Bot(token=os.environ.get('TELEGRAM_BOT_TOKEN'))
    dp = Dispatcher(bot)

    dp.middleware.setup(WhitelistMiddleware())
    dp.register_message_handler(telegram_command_start, commands={"start", "restart"})
    dp.register_message_handler(telegram_command_consumption, commands={"consumption"})
    dp.register_message_handler(telegram_command_status, commands={"status"})
    dp.register_message_handler(telegram_command_enable, commands={"enable"})
    dp.register_message_handler(telegram_command_disable, commands={"disable"})
    dp.register_message_handler(telegram_command_powercycle, commands={"powercycle"})
    dp.register_message_handler(telegram_command_debug, commands={"debug"})

    await bot.set_my_commands([
        BotCommand("consumption", "Get the current power consumption"),
        BotCommand("status", "Get the current status of the watchdog"),
        BotCommand("enable", "Enable the watchdog"),
        BotCommand("disable", "Disable the watchdog"),
        BotCommand("powercycle", "Request a powercycle ⚡"),
    ])

    await dp.start_polling()


async def get_current_consumption() -> int:
    for i in range(3):
        try:
            await device.async_update()
            power_info: PowerInfo = await device.async_get_instant_metrics()
            return int(power_info.power)
        except Exception as ex:
            log_print('Exception: %s' % ex)
            await asyncio.sleep(3)
            continue
    raise PlugConnectionException()


async def watchdog():
    global http_api_client
    http_api_client = await MerossHttpClient.async_from_user_password(email=EMAIL, password=PASSWORD)

    global manager
    manager = MerossManager(http_client=http_api_client)
    await manager.async_init()

    await manager.async_device_discovery()
    plugs = manager.find_devices(device_type="mss310")

    if len(plugs) < 1:
        print("No MSS310 plugs found...")
        return

    global device
    device = plugs[0]

    low_consumption_start_time = None

    global last_watchdog_check

    await telegram_broadcast('Starting watchdog!', True)

    while True:
        try:
            last_watchdog_check = datetime.now()
            current_consumption = await get_current_consumption()
            if current_consumption < consumption_threshold:
                log_print("Current consumption (%s W) is under threshold (%s W)!" % (current_consumption, consumption_threshold))
                if watchdog_enabled and low_consumption_start_time is None:
                    low_consumption_start_time = datetime.now()
                    log_print("Setting new low consumption start time to %s" % low_consumption_start_time)
                    await telegram_broadcast(
                        "⚠️ Warning! Low power consumption detected. A power cycle will be performed "
                        "in %d seconds if it doesn't rise." % max_seconds_low_consumption, False)
            else:
                log_print("Current consumption is %s W" % current_consumption)
                if low_consumption_start_time is not None:
                    await telegram_broadcast("👍 Current power consumption rised over threshold level", False)

                low_consumption_start_time = None

            if low_consumption_start_time is not None:
                low_consumption_seconds_elapsed = (datetime.now() - low_consumption_start_time).total_seconds()
                log_print("Low consumption seconds elapsed: %i" % low_consumption_seconds_elapsed)
                if watchdog_enabled and low_consumption_seconds_elapsed > max_seconds_low_consumption:
                    await power_cycle()
                    low_consumption_start_time = datetime.now()

            delta = (last_watchdog_check + timedelta(0, fetch_seconds_interval) - datetime.now()).total_seconds()
            await asyncio.sleep(delta)
        except:
            manager.close()
            await http_api_client.async_logout()
            await telegram_broadcast('Connection failed. Terminating...', True)
            raise Exception()


def log_print(string):
    print(f'[%s] {string}' % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


async def power_cycle():
    await telegram_broadcast('Performing power cycle!', False)
    await device.async_update()
    await device.async_turn_off()
    await asyncio.sleep(3)
    await device.async_turn_on()
    await telegram_broadcast('Done ⚡', True)


def handle_watchdog_termination(task: asyncio.Task):
    try:
        task.result()
    except asyncio.CancelledError:
        pass  # Task cancellation should not be logged as an error.
    except Exception as e:
        print(e)
        sys.exit(1)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
